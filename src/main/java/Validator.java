import java.util.Arrays;

/**
 * Created by User on 05/11/2016.
 */
public class Validator {

    public Validator(){
    }

    public boolean checkSudoku(int[][] grid){
        if(!checkRow(grid) && !checkSquare(grid) && !checkColumn(grid)){
            return false;
        }
        return true;
    }

    private boolean checkRow(int[][] grid){
        for(int i=0; i <9 ; i++){
            int[] row = grid[i];
            if(!validate(row)){
                return false;
            }
        }
        return true;
    }

    private boolean checkSquare(int[][] grid){
        int[] square= new int[9];
        for(int i = 0 ; i <9 ; i+=3){
            for (int j=0 ; j<9 ; j+=3){
                int counter = 0;
                for (int k=0 ; k<3;k++){
                    for (int n = 0; n<3 ; n++){
                        square[counter++] = grid[k][n];
                    }
                }
                if(!validate(square)){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkColumn(int[][] grid){
        int[] col = new int[9];
        for (int i=0 ; i<9 ; i++){
            for(int j=0;j<9;j++){
                for (int k =0 ; k <9 ; k++){
                    col[j] = grid[k][i];
                }
            }
            if (!validate(col)){
                return false;
            }
        }
        return true;
    }

    private boolean validate(int[] current){
        int i = 1;
        Arrays.sort(current);
        for(int num : current){
            if(num != i++){
                return false;
            }
        }
        return true;
    }
}
